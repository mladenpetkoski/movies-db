-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 07, 2018 at 02:01 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `filmovi`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_filma` varchar(100) CHARACTER SET utf8 NOT NULL,
  `zanr` int(11) NOT NULL,
  `vreme_trajanja` varchar(50) CHARACTER SET utf8 NOT NULL,
  `opis` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_genre` (`zanr`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `naziv_filma`, `zanr`, `vreme_trajanja`, `opis`) VALUES
(1, 'Dr. Strange', 1, '93 minuta', '\r\nA psychiatrist becomes the new Sorcerer Supreme of the Earth in order to battle an evil Sorceress from the past.'),
(2, 'The Twins', 2, '107 minutes', 'A physically perfect but innocent man goes in search of his long-lost twin brother, who is short, a womanizer, and small-time crook.'),
(3, 'The Human Centipede (First Sequence)', 5, '100 minutes', 'A mad scientist kidnaps and mutilates a trio of tourists in order to reassemble them into a human centipede, created by stitching their mouths to each others\' rectums.'),
(4, 'Cloud Atlas', 3, '180', 'An exploration of how the actions of individual lives impact one another in the past, present and future, as one soul is shaped from a killer'),
(5, 'Interstellar', 4, '180min', 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity\'s survival.');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_zanra` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `naziv_zanra`) VALUES
(1, 'Akcija'),
(2, 'Komedija'),
(3, 'Fantazija'),
(4, 'Sci-fi'),
(5, 'Horor');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_filma` int(11) NOT NULL,
  `datum` date NOT NULL,
  `vreme_projekcije` time NOT NULL,
  `bioskop` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_film` (`id_filma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `id_filma`, `datum`, `vreme_projekcije`, `bioskop`) VALUES
(1, 1, '2018-04-10', '19:19:27', 'Lifka'),
(2, 2, '2018-04-27', '20:00:00', 'Radnicki'),
(3, 4, '2018-04-30', '17:00:00', 'Lifka'),
(4, 3, '2018-04-28', '21:00:00', 'Eurocinema'),
(5, 5, '2018-04-24', '20:00:00', 'Lifka');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `id_genre` FOREIGN KEY (`zanr`) REFERENCES `genre` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `id_film` FOREIGN KEY (`id_filma`) REFERENCES `film` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
