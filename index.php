<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Filmovi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/styles.css" />
        <link href="https://fonts.googleapis.com/css?family=Bangers|Fredoka+One&amp;subset=latin-ext" rel="stylesheet">
    </head>
    <body>
        <div class="content">
        <?php 
        include("inc/header.html"); 
        include("inc/aside.html"); 
        
        if(!isset($_GET["link"])) 
        $_GET["link"]="index"; 
        
        switch ($_GET["link"]) {   
            
            case "film":  
            include("inc/film.php");  
            break;  
            
            case "genre":  
            include("inc/genre.php");  
            break;  
            
            case "schedule":  
            include("inc/schedule.php");  
            break; 
            
            default:  
            include("inc/film.php");  
            break; 

            } 
        include("inc/footer.html"); ?>
        </div>
    </body>
</html>